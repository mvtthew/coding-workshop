# Summer Coding Workshop - Welcome!

This is a git repository, and it will be the source material for everything we
work on together.

##### Table of Contents

- Getting Started
- [Team Members](#team-members)
- [Git](#git)
- [FAQ](#frequently-asked-questions)

------------------------------------------------------------------------------------------------------------------------

## Getting Started

1. [ ] [Download & install git.](https://git-scm.com/downloads)
2. [ ] [Create an account at GitLab.](https.about.gitlab.com)
3. [ ] [Download & install Atom.](https://atom.io/)
4. [ ] [Create an account at Codecademy.](https://www.codecademy.com/)
5. [ ] Configure git on your machine.

To configure your machine, open a terminal and use the following commands:

```
$ git config --global user.name {your name in quotation marks}
$ git config --global user.email {the email address you used for GitLab}
```

#### E.g.

```
$ git config --global user.name "Matthew Yanez"

$ git config user.name
> Matthew Yanez

$ git config --global user.email matthew.yanez@asu.edu

$ git config user.email
> matthew.yanez@asu.edu
```

------------------------------------------------------------------------------------------------------------------------

## Team Members

- [Briana](/docs/01-team/briana.md)
- [Effy](/docs/01-team/effy.md)
- [Gaby](/docs/01-team/gaby.md)
- [Natalie](/docs/01-team/natalie.md)
- [Oliver](/docs/01-team/oliver.md)
- [Pri](/docs/01-team/pri.md)
- [Sarina](/docs/01-team/sarina.md)
- [Matt](/docs/01-team/matt.md)

> Notice that your name links out to a `.md` (read "dot-markdown") file located
> under the `/docs/01-team` folder. Open the file underneath your folder and
> write a brief about-me for youself. Include something you're proud
> of about yourself, why you're interested in participating in this workshop, and
> what you hope to accomplish here.

> Feel free to try out Markdown to add styling to your page. GitLab's Markdown
> rules are described
> [here](https://docs.gitlab.com/ee/user/markdown.html#blockquotes).

------------------------------------------------------------------------------------------------------------------------

## Git

Git is a **version control system**. It is a software application that helps us
save and track changes to a collection of documents over time.

> Navigate to the Git module on
> [Codecademy](https://www.codecademy.com/courses/learn-git) and get started.

------------------------------------------------------------------------------------------------------------------------

## Frequently Asked Questions
